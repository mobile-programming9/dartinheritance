import 'dart:io';

class BoiledEgg {
  int minute;
  int heat;
  int numegg;

  BoiledEgg(this.minute, this.heat, this.numegg);

  void callEgg() {
    if (numegg < 4) {
      if (minute >= 0 && minute <= 1) {
        if (heat >= 0 && heat <= 20) {
          print("heat is: $heat");
          print("in time: $minute"
              " minute and use heat: $heat"
              " Eggs are still raw!!!");
        } else if (heat >= 21 && heat <= 40) {
          print("heat is: $heat");
          print("in time: $minute"
              " minute and use heat: $heat"
              " Egg whites begin to cook!!!");
        } else if (heat >= 41 && heat <= 60) {
          print("heat is: $heat");
          print("in time: $minute"
              " minute and use heat: $heat"
              " Cook egg whites!!!");
        } else if (heat >= 61 && heat <= 80) {
          print("heat is: $heat");
          print("in time: $minute"
              " minute and use heat: $heat"
              " Cooked egg yolk!!!");
        } else if (heat >= 81 && heat <= 100) {
          print("heat is: $heat");
          print("in time: $minute"
              " minute and use heat: $heat"
              " Boiled egg!!!");
        }
      }
    }

    if (minute >= 2 && minute <= 3) {
      if (heat >= 0 && heat <= 20) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Egg whites begin to cook!!!");
      } else if (heat >= 21 && heat <= 40) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Cook egg whites!!!");
      } else if (heat >= 41 && heat <= 60) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " A soft boiled egg!!!");
      } else if (heat >= 61 && heat <= 80) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " An onsen egg!!!");
      } else if (heat >= 81 && heat <= 100) {
        print("heat is: $heat");
        print(
            "in time: $minute" " minute and use heat: $heat" " Boiled egg!!!");
      }
    }

    if (minute >= 5 && minute <= 7) {
      if (heat >= 0 && heat <= 20) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " A soft boiled egg!!!");
      } else if (heat >= 21 && heat <= 40) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " An onsen eggs!!!");
      } else if (heat >= 41 && heat <= 60) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Scrambled eggs!!!");
      } else if (heat >= 61 && heat <= 80) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Almost cooked egg!!!");
      } else if (heat >= 81 && heat <= 100) {
        print("heat is: $heat");
        print(
            "in time: $minute" " minute and use heat: $heat" " Boiled egg!!!");
      }
    }

    if (minute >= 9 && minute <= 11) {
      if (heat >= 0 && heat <= 20) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Soft boiled egg!!!");
      } else if (heat >= 21 && heat <= 40) {
        print("heat is: $heat");
        print("in time: $minute" " minute and use heat: $heat" " Onsen egg!!!");
      } else if (heat >= 41 && heat <= 60) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Scrambled egg!!!");
      } else if (heat >= 61 && heat <= 80) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Almost cooked egg!!!");
      } else if (heat >= 81 && heat <= 100) {
        print("heat is: $heat");
        print(
            "in time: $minute" " minute and use heat: $heat" " Boiled egg!!!");
      }
    }

    if (minute >= 13 && minute <= 15) {
      if (heat >= 0 && heat <= 20) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Scrambled egg!!!");
      } else if (heat >= 21 && heat <= 40) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Almost cooked egg!!!");
      } else if (heat >= 41 && heat <= 60) {
        print("heat is: $heat");
        print(
            "in time: $minute" " minute and use heat: $heat" " Boiled egg!!!");
      } else if (heat >= 61 && heat <= 80) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Overcook egg!!!");
      } else if (heat >= 81 && heat <= 100) {
        print("heat is: $heat");
        print("in time: $minute"
            " minute and use heat: $heat"
            " Exploding egg!!!");
      }
    } else {
      print("The number of eggs exceeded the limit!!!");
    }
  }

  int getMinute() {
    return minute;
  }

  int getHeat() {
    return heat;
  }

  int getNumEgg() {
    return numegg;
  }

  void setMinute(int minute) {
    this.minute = minute;
  }

  void setHeat(int heat) {
    this.heat = heat;
  }

  void setNumEgg(int numegg) {
    this.numegg = numegg;
  }

  void showEgg() {
    print("eggs boiling time: $minute"
        " heat boiling eggs: $heat"
        "number if eggs: $numegg");
  }
}
